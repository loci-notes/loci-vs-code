import * as vscode from 'vscode';
import dayjs from 'dayjs';

import * as utils from './utils';
import * as crud from './crud';
import * as client from './client';
import * as pathUtils from 'path';
import * as ext from './extension';
import { ArtifactNote, ArtifactNoteIn, ArtifactOut, ArtifactPriorityEnum, UserOut } from './client';
import { lociSemaphore } from './utils';

import RelativeTime from 'dayjs/plugin/relativeTime';
import utc from 'dayjs/plugin/utc';
dayjs.extend(RelativeTime);
dayjs.extend(utc);

export class NoteComment implements vscode.Comment {
    label: string | undefined;

    constructor(
        public id: number,
        public body: vscode.MarkdownString,
        public mode: vscode.CommentMode,
        public author: vscode.CommentAuthorInformation,
        public parent?: vscode.CommentThread,
        public contextValue?: string
    ) {
        this.label = undefined;
    }
}

export async function addNote(reply: vscode.CommentReply, artifactPriority: ArtifactPriorityEnum) {
    if (vscode.window.activeTextEditor) {
        // Very first thing we do is put in a dummy comment. We get some weird jumping and fail to clear the input if we don't do this. It gets cleared by a thread refresh almost immediately.
        const fakeMdBody = new vscode.MarkdownString("Working...");
        const fakeComment = new NoteComment(-1, fakeMdBody, vscode.CommentMode.Preview, { name: "" }, reply.thread, '');
        reply.thread.comments = [...reply.thread.comments, fakeComment];

        var currentSelection = reply.thread.range;
        var artifact = utils.commentReplyToArtifactFilename(reply, true) + ":" + (currentSelection.start.line + 1);
        if (!currentSelection?.isSingleLine) {
            artifact += "-" + (currentSelection.end.line + 1);
        }

        var lociClient = utils.getLociClient();
        var ani: ArtifactNoteIn = {
            artifact_descriptor: artifact,
            submission_tool: "VS Code",
            type: "COMMENT",
            contents: reply.text,
            artifact_type: "SOURCE_CODE_LOCATION",
            artifact_priority: artifactPriority,
        };

        const [value, release] = await lociSemaphore.acquire();
        lociClient.createNote(utils.getProjectId(), ani).then(function (response) {
            var artifactNote = response.data as ArtifactNoteIn;

            // Success, update the comment thread.
            if (reply.thread.contextValue === undefined) {
                // This is a brand new comment thread, we need to set it up properly.

                // Get the artifact ID from the response.
                var artifactId = artifactNote.artifact_id;
                if (artifactId !== undefined && artifactId !== null) {
                    reply.thread.contextValue = artifactId?.toString();

                    // Retain a snapshot of the source code attached to this comment, for later comparison among different versions of the same file. In theory, this can help us transfer notes as the code changes over time.
                    // We need to loop through all open editors, as the "active" one is a comment. and not actually the document.
                    var editor: vscode.TextEditor | undefined = undefined;
                    for (var i = 0; i < vscode.window.visibleTextEditors.length; i++) {
                        if (vscode.window.visibleTextEditors[i].document.uri.path === reply.thread.uri.path) {
                            editor = vscode.window.visibleTextEditors[i];
                            break;
                        }
                    }

                    function createSourceCodeTxt(text: String | undefined): void {
                        if (sourceCodeTxt === undefined) {
                            vscode.window.showErrorMessage("Failed to create a text snapshot of the code, the source code was not found. This is probably a bug.");
                            return;
                        }
                        var snapshotNote: ArtifactNoteIn = {
                            artifact_descriptor: artifact,
                            submission_tool: "VS Code",
                            type: "SNAPSHOT_TXT",
                            artifact_type: "SOURCE_CODE_LOCATION",
                            artifact_priority: "MEDIUM",
                            contents: sourceCodeTxt,
                        };
                        lociClient.createNote(utils.getProjectId(), snapshotNote).then(function (response) {
                            // Success, do nothing.
                        }).catch(function (error) {
                            vscode.window.showErrorMessage("Failed to create a snapshot of the source code, the note could not be created. This is probably a bug.");
                        });
                    }
                    
                    var sourceCodeTxt: string | undefined = undefined;
                    if (editor === undefined) {
                        // We need to figure out the contents of the comment, even if it's not currently open (which can happen if this is called from something like a search result).
                        // Get the text of the document from the URI.
                        var doc = vscode.workspace.openTextDocument(reply.thread.uri).then((doc) => {
                            var range = new vscode.Range(currentSelection.start, new vscode.Position(currentSelection.end.line, doc.lineAt(currentSelection.end.line).range.end.character));
                            sourceCodeTxt = doc.getText(range);
                            createSourceCodeTxt(sourceCodeTxt);
                        });
                        
                    } else {
                        // The range of the entire line of the comment.
                        var range = new vscode.Range(currentSelection.start, new vscode.Position(currentSelection.end.line, editor.document.lineAt(currentSelection.end.line).range.end.character));
                        sourceCodeTxt = editor.document.getText(range);
                        createSourceCodeTxt(sourceCodeTxt);
                    }
                    
                }
                else {
                    vscode.window.showErrorMessage("Failed to create note, the artifact ID was not returned. This is probably a bug.");
                    return;
                }
            }
            // Refresh only this comment thread.
            vscode.commands.executeCommand("loci-notes-vs-code.refresh-artifact-thread-and-notes", reply.thread);
        }).finally(() => {
            release();
        });
    }
    else {
        // We should never be here.
        vscode.window.showErrorMessage("Failed to create note, nothing appears to be selected. This is probably a bug.");
        return;
    }
}

export async function refreshSingleArtifactThread(thread: vscode.CommentThread, refreshNotes: boolean = true) {
    var artifactId = thread.contextValue;
    if (artifactId === undefined) {
        vscode.window.showErrorMessage("Failed to refresh artifact thread, the artifact ID was not found. This is probably a bug.");
        return;
    }
    
    // Get the artifact info
    var lociClient = utils.getLociClient();
    const [value, release] = await lociSemaphore.acquire();
    var response = await lociClient.readArtifact(Number(artifactId)).finally(() => {
        release();
    });

    var artifact = response.data as ArtifactOut;

    var artifactDescriptor = artifact.descriptor;
    var artifactPriorityStr = utils.priorityToUserString(artifact.priority);
    thread.label = `[${artifactPriorityStr}] ${artifactDescriptor}`;

    if (refreshNotes) {
        // Here, we are pre-initializaing the array of comments that is going to replace the one that already exists. If we do this one by one, there's some lag as each comment gets added, causing our place to jump around, which is generally unpleasant and causes us to lose our place.
        var newComments = [] as vscode.Comment[];

        // Once we have an artifact, quickly add a placeholder comment to the thread until we have the real ones. This is much smoother than waiting for the real ones to load. Theres some weird jumping behavior from the cursor among the input fields of each of the comments if we don't do this.
        var fakeComments = [] as vscode.Comment[];
        var fakeComment = new NoteComment(-1, new vscode.MarkdownString("Working..."), vscode.CommentMode.Preview, { name: "" }, thread, '');
        fakeComments.push(fakeComment);
        thread.comments = fakeComments;

        var authorsCache = new Map<number, UserOut>();

        for (var i = 0; i < artifact.note_ids.length; i++) {
            var noteId = artifact.note_ids[i];
            // Technically we should probably do this asyncronously, but it's guarenteed to be in order this way.
            const [value, release] = await lociSemaphore.acquire();
            var response2 = await lociClient.readNote(noteId).finally(() => {
                release();
            });

            var note = response2.data as ArtifactNote;

            // Figure out what kind of note it is, and if we are going to display it.
            var noteType = note.type;
            if (noteType !== client.ArtifactNoteTypeEnum.Comment) {
                continue;
            }
            // Only COMMENT notes are currently displayed.
            // TODO Check to see if SNAPSHOT_TXT notes are included and correct. Warn the user if incorrect.

            // Here, it's possible that the contents of the link contains a markdown link to either another artifact or to a different location somewhere that is not itself an artifact. We need to handle this by switching out the contents of the link with a correct one that VS Code knows how to handle.
            var noteContents = utils.processNoteLinks(note.contents);

            var mdBody = new vscode.MarkdownString(noteContents);
            if (note.id === undefined || note.id === null || note.author_id === undefined || note.author_id === null) {
                vscode.window.showErrorMessage("Failed to read note, the note ID or user ID was not found. This is probably a bug.");
                return;
            }
            // Get the author info (if needed)
            var authorId = note.author_id;
            if (!authorsCache.has(authorId)) {
                var response3 = await lociClient.readUserById(authorId);
                var author = response3.data as UserOut;
                authorsCache.set(authorId, author);
            }
            var authorObj = authorsCache.get(authorId);
            if (authorObj === undefined) {
                vscode.window.showErrorMessage("Failed to read author info, the author ID was not found. This is probably a bug.");
                return;
            }
            var authorName = authorObj.full_name;
            if (authorName === undefined || authorName === null || authorName === "") {
                authorName = authorObj.email;
            }
            var newComment = new NoteComment(note.id, mdBody, vscode.CommentMode.Preview, { name: authorName }, thread, 'canDelete');

            var tool = note.submission_tool;
            if (tool === undefined || tool === null || tool === "") {
                tool = "Unknown tool";
            }

            var createdAtTime = "";
            if (note.created_at === undefined || note.created_at === null) {
                createdAtTime = "Unknown time";
            }
            else {
                var now = dayjs().utc();
                var then = dayjs.utc(note.created_at);
                createdAtTime = then.from(now);
            }

            newComment.label = " in " + note.submission_tool + ", " + createdAtTime;
            newComments.push(newComment);
        }
        
        // Replace the comments all at once. No idea why this matters, but if we do it one by one, we get a weird artifact where the comment never leaves the input area, even after we submit it, plus stuttering as the comments get added.
        thread.comments = newComments;
    }
}

/**
 * Get the first (or only) line number of the artifact descriptor. This is the starting line number in the file where the artifact is located.
 * 
 * @param artifact 
 * @returns 
 */
export function getArtifactLineStart(artifact: ArtifactOut): number | undefined {
    return getArtifactLineStartByString(artifact.descriptor);
}

/**
 * Get the first (or only) line number of the artifact descriptor. This is the starting line number in the file where the artifact is located.
 * 
 * @param artifact 
 * @returns 
 */
export function getArtifactLineStartByString(artifactDescriptor: string): number | undefined {
    var lineStr = artifactDescriptor.split(":")[1];
    if (lineStr === undefined) {
        return undefined;
    }
    return parseInt(lineStr.split("-")[0]);
}

/**
 * Get the second line number of the artifact descriptor. If the descriptor is only one line, this will be undefined.
 * 
 * @param artifact 
 * @returns 
 */
export function getArtifactLineEnd(artifact: ArtifactOut): number | undefined {
    return getArtifactLineEndByString(artifact.descriptor);
}

/**
 * Get the second line number of the artifact descriptor. If the descriptor is only one line, this will be undefined.
 * 
 * @param artifact 
 * @returns 
 */
export function getArtifactLineEndByString(artifactDescriptor: string): number | undefined {
    var lineStr = artifactDescriptor.split(":")[1];
    if (lineStr === undefined) {
        return undefined;
    }

    var splitArr = lineStr.split("-");
    if (splitArr.length < 2) {
        return undefined;
    }

    return parseInt(splitArr[1]);
}

/**
 * Transforms a local filename into an artifact filename. Turns a local filename into a filename relative to the source root directory. It also handles Windows-style folder slashes and multi-root workspaces.
 * 
 * @param artifact the artifact object
 * @returns a URI pointing to the exact location of the artifact, line numbers included.
 */
export function getArtifactLocalUri(artifact: ArtifactOut, includeLines: boolean = true): vscode.Uri | undefined {
    return getArtifactLocalUriByString(artifact.descriptor, includeLines);
}

/**
 * Transforms a local filename into an artifact filename. Turns a local filename into a filename relative to the source root directory. It also handles Windows-style folder slashes and multi-root workspaces.
 * 
 * @param artifact the artifact object
 * @returns a URI pointing to the exact location of the artifact, line numbers included.
 */
export function getArtifactLocalUriByString(artifactDescriptor: string, includeLines: boolean = true): vscode.Uri | undefined {
    const descriptorFilePart = artifactDescriptor.split(":")[0];
    const startLine: number | undefined = getArtifactLineStartByString(artifactDescriptor);

    if (startLine === undefined) {
        return undefined;
    }
    if (descriptorFilePart === undefined) {
        return undefined;
    }

    var matches: string[] = [];

    ext.fqFilesIndex.forEach((indexFile) => {
        // We convert it into a normalized path because we have no idea what OS the user is on. Source code artifacts use POSIX paths.
        var normalizedFilePath = pathUtils.normalize(indexFile).split(pathUtils.sep).join(pathUtils.posix.sep);
        if (normalizedFilePath.endsWith(descriptorFilePart)) {
            matches.push(indexFile);
        }
    });

    // Correct and best case, we expect only one match.
    if (matches.length === 0) {
        // vscode.window.showWarningMessage(`Could not find a matching file for the artifact '${artifactDescriptor}'.`);
        return undefined;
    }
    if (matches.length > 1) {
        // Do some extra processing to figure out where this artifact refers to.
        var betterDescriptorFilePart: string = "/" + utils.getLociSourceDirectoryName() + "/" + descriptorFilePart;
        var matches2: string[] = [];

        matches.forEach((matchFile) => {
            // We convert it into a normalized path because we have no idea what OS the user is on. Source code artifacts use POSIX paths.
            var normalizedFilePath = pathUtils.normalize(matchFile).split(pathUtils.sep).join(pathUtils.posix.sep);
            if (normalizedFilePath.endsWith(betterDescriptorFilePart)) {
                matches2.push(matchFile);
            }
        });

        // Correct and best case, we expect only one match.
        if (matches2.length === 0) {
            // vscode.window.showWarningMessage(`Could not find a exact matching file for the artifact '${artifactDescriptor}' despite some possibilities. This is likely a bug that should be corrected.`);
            return undefined;
        }

        if (matches2.length > 1) {
            vscode.window.showWarningMessage(`Found multiple files matching the artifact '${artifactDescriptor}' despite multiple checks. This could mean your environment may have duplicate repos, or is otherwise in an incorrect state.`);
            return undefined;
        }

        matches = matches2;
    }
    var tmpUri = vscode.Uri.file(matches[0]);

    if (includeLines) {
        return vscode.Uri.parse(tmpUri.path + "#" + startLine);
    }
    else {
        return tmpUri;
    }
}