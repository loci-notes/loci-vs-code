import * as utils from "./utils";
import { lociSemaphore } from "./utils";

// TODO May want to bump this up if we have trouble getting lots of artifacts from the server.
const LIMIT = 100;

import { ArtifactOut, ArtifactPriorityEnum, ArtifactsOut } from "./client";
import { Axios, AxiosResponse } from "axios";
/**
 * Get a list of all the source code files in the project.
 * 
 * @returns A list of all the source code files in the project.
 */
export async function getProjectFiles(): Promise<ArtifactOut[]> {
    var lociClient = utils.getLociClient();
    var projectId = utils.getProjectId();

    // Recursion AND async programming, it must be my birthday.
    async function _getProjectFiles(offset: number): Promise<ArtifactOut[]> {
        const [value, release] = await lociSemaphore.acquire();
        var artifactsOut = await lociClient.readProjectArtifacts(
            projectId,
            offset,
            LIMIT,
            null,
            null,
            "descriptor",
            "asc",
            undefined,
            "SOURCE_CODE_FILE",
        ).then(function (response) {
            var artifactsOut = response.data as ArtifactsOut;
            return artifactsOut;
        }).finally(() => {
            release();
        });
        var numRecordsReturned = artifactsOut.data.length;
        if (artifactsOut.count > offset + numRecordsReturned) {
            return artifactsOut.data.concat(await _getProjectFiles(offset + numRecordsReturned));
        }
        return artifactsOut.data;
    }

    var files = await _getProjectFiles(0);
    return files;
}

/**
 * Get a list of the source code locations in the project where status is equal to whatever is passed via the status parameter.
 * 
 * @returns A list of all the source code locations in the project.
 */
export async function getLocationsByPriority(priority: ArtifactPriorityEnum): Promise<ArtifactOut[]> {
    var lociClient = utils.getLociClient();
    var projectId = utils.getProjectId();

    async function _getArtifactsByPriority(offset: number): Promise<ArtifactOut[]> {
        const [value, release] = await lociSemaphore.acquire();
        var artifactsOut = await lociClient.readProjectArtifacts(
            projectId,
            offset,
            LIMIT,
            null,
            null,
            "descriptor",
            "asc",
            undefined,
            "SOURCE_CODE_LOCATION",
            priority,
        ).then(function (response) {
            var artifactsOut = response.data as ArtifactsOut;
            return artifactsOut;
        }).finally(() => {
            release();
        });

        var numRecordsReturned = artifactsOut.data.length;
        if (artifactsOut.count > offset + numRecordsReturned) {
            return artifactsOut.data.concat(await _getArtifactsByPriority(offset + numRecordsReturned));
        }
        return artifactsOut.data;
    }

    var artifacts = await _getArtifactsByPriority(0);
    return artifacts;
}

/**
 * Get a list of all the source code locations in the project.
 * 
 * @returns A list of all the source code locations in the project .
 */
export async function getAllLocations(): Promise<ArtifactOut[]> {
    var lociClient = utils.getLociClient();
    var projectId = utils.getProjectId();

    async function _getAllLocations(offset: number): Promise<ArtifactOut[]> {
        const [value, release] = await lociSemaphore.acquire();
        var artifactsOut = await lociClient.readProjectArtifacts(
            projectId,
            offset,
            LIMIT,
            null,
            null,
            "descriptor",
            "asc",
            undefined,
            "SOURCE_CODE_LOCATION",
        ).then(function (response) {
            var artifactsOut = response.data as ArtifactsOut;
            return artifactsOut;
        }).finally(() => {
            release();
        });
        var numRecordsReturned = artifactsOut.data.length;
        if (artifactsOut.count > offset + numRecordsReturned) {
            return artifactsOut.data.concat(await _getAllLocations(offset + numRecordsReturned));
        }
        return artifactsOut.data;
    }

    var artifacts = await _getAllLocations(0);
    return artifacts;
}