import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import * as utils from './utils';
import * as crud from './crud';
import { ArtifactPriorityEnum } from './client';
import { ArtifactOut } from './client';

export class ArtifactListView implements vscode.TreeDataProvider<ArtifactListItem> {
    private _onDidChangeTreeData: vscode.EventEmitter<ArtifactListItem | undefined | null | void> = new vscode.EventEmitter<ArtifactListItem | undefined | null | void>();
    readonly onDidChangeTreeData: vscode.Event<ArtifactListItem | undefined | null | void> = this._onDidChangeTreeData.event;

    constructor() { }

    refresh(): void {
        this._onDidChangeTreeData.fire();
    }

    getTreeItem(element: ArtifactListItem): vscode.TreeItem {
        return element;
    }

    getChildren(element?: ArtifactListItem): Thenable<ArtifactListItem[]> {
        if (element === undefined) {
            // This means it is a top-level state identifier list item
            var highTree = new ArtifactListItem("HIGH", "High priority artifacts", vscode.TreeItemCollapsibleState.Expanded, ArtifactPriorityEnum.High);
            var mediumTree = new ArtifactListItem("MEDIUM", "Medium priority artifacts", vscode.TreeItemCollapsibleState.Collapsed, ArtifactPriorityEnum.Medium);
            var lowTree = new ArtifactListItem("LOW", "Low priority artifacts", vscode.TreeItemCollapsibleState.Collapsed, ArtifactPriorityEnum.Low);
            var noneTree = new ArtifactListItem("NONE", "Completed artifacts (no priority)", vscode.TreeItemCollapsibleState.Collapsed, ArtifactPriorityEnum.None);

            return Promise.resolve([highTree, mediumTree, lowTree, noneTree]);

        } else {
            // This means we need to populate the artifact (source code location) list for a given artifact status.
            // TODO This isn't really the best way to handle this for now, but we can work out how to arrange these better in the future.
            const projectId = utils.getProjectId();
            const thisStateTree = element.main;
            var fileList: ArtifactListItem[] = [];

            return crud.getLocationsByPriority(thisStateTree as ArtifactPriorityEnum).then(function (files) {
                for (var i = 0; i < files.length; i++) {
                    var fileItem = new ArtifactListItem(files[i].descriptor, "", vscode.TreeItemCollapsibleState.None, thisStateTree, files[i]);
                    fileList.push(fileItem);
                }
                return Promise.resolve(fileList);
            });
        }
    }
}

class ArtifactListItem extends vscode.TreeItem {
    constructor(
        public readonly main: string,
        public readonly description: string,
        public readonly collapsibleState: vscode.TreeItemCollapsibleState,
        public readonly level: string,
        public readonly artifactObj?: ArtifactOut
    ) {
        super(main, collapsibleState);
        this.tooltip = `${this.main}`;
        this.description = description;
        this.level = level;
        if (artifactObj !== undefined) {
            // cmdArg should hold the full artifact descriptor
            this.command = {
                command: 'loci-notes-vs-code.open-artifact',
                title: '',
                arguments: [artifactObj]
            };
        }
    }
}
