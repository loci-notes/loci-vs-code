import * as vscode from 'vscode';
import * as utils from './utils';
import * as crud from './crud';
import * as objs from './objs';
import { lociSemaphore } from './utils';
const path = require('node:path');
import { ArtifactListView } from './ArtifactListView';
import { ArtifactOut, ArtifactPriorityEnum, ProjectOut, UserFullOut } from './client';
import { NavStack } from './NavStack';
import { NavStackView } from './NavStackView';

let disposables: vscode.Disposable[] = [];
export let fqFilesIndex: string[] = [];

const FILE_LINE_REGEX = /^(\S.*):$/;
const RESULT_LINE_REGEX = /^(\s+)(\d+)(:) (.*)$/;

var navStack = new NavStack();

function getCurrentCursorLocation(): string | undefined {
	if (!vscode.window.activeTextEditor) {
		return undefined;
	}

	var filename = utils.openDocumentToArtifactFilename();
	var startRange = vscode.window.activeTextEditor.selection.start;
	var endRange = vscode.window.activeTextEditor.selection.end;

	var lines = "";
	if (startRange.line === endRange.line) {
		lines = (startRange.line + 1).toString();
	}
	else {
		lines = (startRange.line + 1) + "-" + (endRange.line + 1);
	}

	var location = filename + ":" + lines;
	return location;
}

// This method is called when your extension is activated
export function activate(context: vscode.ExtensionContext) {
	// The comment controller is used the component that handles adding comments to lines of code.
	var commentController = newCommentController();

	// This command opens up the online docs for Loci Notes VS Code
	vscode.commands.registerCommand('loci-notes-vs-code.help', () => {
		var helpUri = vscode.Uri.parse("https://loci-notes.gitlab.io/clients/vs-code");
		vscode.commands.executeCommand("vscode.open", helpUri);
	});

	context.subscriptions.push(vscode.commands.registerCommand('loci-notes-vs-code.add-note-as-text', (reply: vscode.CommentReply) => {
		objs.addNote(reply, "MEDIUM");
	}));


	// This command updates the cached listing of files we maintain so that we don't have to call vscode.workspace.findFiles() every time we want to see what files we have. The reason we cache it is that the listing of files is not expected to rapidly change, and findFiles is asynchronous, which screws up the flow of later calls.
	vscode.commands.registerCommand('loci-notes-vs-code.refresh-files-index', () => {
		// Clear out the old index.
		fqFilesIndex = [] as string[];

		// This code will get a full listing of all files in the workspace and save it for later, in a synchronous way.
		if (vscode.workspace.workspaceFolders === undefined) {
			vscode.window.showWarningMessage("Failed for enumerate files due to missing workspace folders. Unless you are not in a workspace, this is probably a bug.");
		}
		else {
			vscode.window.showInformationMessage("Refreshing all Loci Notes artifacts in workspace...");
			var folders = vscode.workspace.workspaceFolders;

			// These should probably be defined somewhere else.
			// Define ignore patterns
			const ignoreFiles = ['**/.DS_STORE'];
			const ignoreDirs = ['**/.git/**'];

			// Create a glob pattern to ignore files and directories
			const ignorePattern = `{${ignoreFiles.join(',')},${ignoreDirs.join(',')}}`;

			// Use vscode.workspace.findFiles to get all files, excluding the ignored ones
			vscode.workspace.findFiles('**/*', ignorePattern).then(files => {
				// let fqFilesIndex: string[] = [];
	
				files.forEach(file => {
					fqFilesIndex.push(file.fsPath);
				});
				
				// We have to put this after the file index refresh, because the artifact list view depends on the file index.
				alv.refresh();
				refreshAllFileArtifacts();
				vscode.window.showInformationMessage("Artifact refresh completed. All Loci Notes information is up-to-date.");
			});
		}
	});

	// This command runs a basic check to make sure the project is setup correctly. It will also check the integrity of all local source code files.
	vscode.commands.registerCommand('loci-notes-vs-code.check-project', () => {
		var client = utils.getLociClient();
		lociSemaphore.acquire().then(function ([value, release]) {
			client.readUserMe().then(function (response) {
				release();
				var userInfo = response.data as UserFullOut;
				var username = userInfo.full_name;
				if (username === undefined || username === null || username === '') {
					username = userInfo.email;
				}
				client.readProject(utils.getProjectId()).then(function (response2) {
					var projectInfo = response2.data as ProjectOut;
					var projectName = projectInfo.name;
					vscode.window.showInformationMessage(`Loci Notes project \'${projectName}\' active as \'${username}\'`);
				});
			}).catch(function (error) {
				vscode.window.showErrorMessage(`Error: ${error}`);
			});
		});

		// Check the integrity of all the source code files. If anything is missing or incorrect, we'll let the user know.
		crud.getProjectFiles().then(function (serverFileList) {
			var localFileList = fqFilesIndex;
			var sourceFileList = [] as string[];


			// Loop through all files from the server check if they are in the project source directory.
			var sourceDirName = utils.getLociSourceDirectoryName();

			for (var i = 0; i < localFileList.length; i++) {
				// First make sure this is actually a file under the source directory...
				var splitPath = localFileList[i].split(path.sep);
				if (splitPath.includes(sourceDirName)) {
					var artifactName = utils.getArtifactFilename(localFileList[i]);
					if (artifactName !== undefined) {
						sourceFileList.push(artifactName);
					}
				}
			}

			// TODO Check for extra files, and warn on that.
			// TODO Check for out-of-date files via hashes, and warn on that.
		});
	});

	// Register the Artifact list view. This is the sidebar on the left that shows a list of artifacts, grouped by status.
	var alv = new ArtifactListView();
	vscode.window.registerTreeDataProvider(
		'artifact-list',
		alv
	);

	// Register the NavStack view.
	var nsv = new NavStackView(navStack);
	vscode.window.registerTreeDataProvider(
		'nav-stack',
		nsv
	);

	var navStackDecorators = vscode.window.createTextEditorDecorationType({
		light: {
			// used for light colored themes
			gutterIconPath: context.asAbsolutePath("docs/imgs/bookmark-dark.svg"),
			gutterIconSize: "contain",
		},
		dark: {
			// used for dark colored themes
			gutterIconPath: context.asAbsolutePath("docs/imgs/bookmark-light.svg"),
			gutterIconSize: "contain",
		}
	});

	function updateDecorationsInCurrentDocument() {
		if (!vscode.window.activeTextEditor) {
			return;
		}

		// Clear out the old decorations.
		vscode.window.activeTextEditor?.setDecorations(navStackDecorators, [] as vscode.DecorationOptions[]);

		var currentDocument = vscode.window.activeTextEditor.document;
		var currentOpenFilename = currentDocument.fileName;

		const navStackDecItems: vscode.DecorationOptions[] = [];
		var navStackItems = navStack.getItems();
		for (var i = 0; i < navStackItems.length; i++) {
			const location = navStackItems[i];
			// This removes the line number from the location string.
			const splitPath = location.split(":");
			if (!currentOpenFilename.includes(splitPath[0])) {
				// We only want to show the nav stack decorators in the current document.
				continue;
			}

			var lineStart = objs.getArtifactLineStartByString(location);
			var lineEnd = objs.getArtifactLineEndByString(location);
			if (lineStart === undefined) {
				vscode.window.showErrorMessage(`Failed to refresh nav stack view due to undefined line start. This is probably a bug.`);
				return;
			}
			if (lineEnd === undefined) {
				lineEnd = lineStart;
			}
			const range = new vscode.Range(lineStart - 1, 0, lineEnd - 1, 0);

			const decoration = { range: range };
			navStackDecItems.push(decoration);
		}
		vscode.window.activeTextEditor?.setDecorations(navStackDecorators, navStackDecItems);
	}

	vscode.commands.registerCommand('loci-notes-vs-code.refresh-nav-stack-view', () => {
		updateDecorationsInCurrentDocument();
		nsv.refresh();
	});

	// There two are triggered when the active text editor changes. They are used to update the nav stack decorators.
	vscode.window.onDidChangeActiveTextEditor(editor => {
		if (editor) {
			updateDecorationsInCurrentDocument();
		}
	}, null, context.subscriptions);

	vscode.workspace.onDidChangeTextDocument(event => {
		if (vscode.window.activeTextEditor && event.document === vscode.window.activeTextEditor.document) {
			updateDecorationsInCurrentDocument();
		}
	}, null, context.subscriptions);

	vscode.commands.registerCommand('loci-notes-vs-code.push-nav-stack', () => {
		if (vscode.window.activeTextEditor) {
			var location = getCurrentCursorLocation();
			if (location === undefined) {
				vscode.window.showErrorMessage("Failed to push nav stack due to undefined location. This is probably a bug.");
				return;
			}
			navStack.push(location);
			vscode.commands.executeCommand('loci-notes-vs-code.refresh-nav-stack-view');
			vscode.window.showInformationMessage(`Pushed ${location} to navigation stack.`);
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.peek-nav-stack', () => {
		if (vscode.window.activeTextEditor) {
			var location = navStack.peek();
			if (location === undefined) {
				vscode.window.showErrorMessage("Failed to peek nav stack due to undefined location. This is probably a bug.");
				return;
			}
			vscode.commands.executeCommand("vscode.open", objs.getArtifactLocalUriByString(location));
			vscode.window.showInformationMessage(`Peeked at ${location} in navigation stack.`);
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.pop-nav-stack', () => {
		if (vscode.window.activeTextEditor) {
			var location = navStack.pop();
			if (location === undefined) {
				vscode.window.showErrorMessage("Failed to pop nav stack due to undefined location. This is probably a bug.");
				return;
			}
			vscode.commands.executeCommand('loci-notes-vs-code.refresh-nav-stack-view');

			// When we remove an item from the nav stack, we should also peek at the next item, so that the user can see where they are going next.
			location = navStack.peek();
			if (location === undefined) {
				// If there is no next item, we don't need to do anything.
				return;
			}
			vscode.commands.executeCommand("vscode.open", objs.getArtifactLocalUriByString(location));
			vscode.window.showInformationMessage(`Popped ${location} from navigation stack.`);
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.refresh-artifacts', () => {
		vscode.commands.executeCommand('loci-notes-vs-code.refresh-files-index');
		
	});

	vscode.commands.registerCommand('loci-notes-vs-code.refresh-artifact-thread', (thread: vscode.CommentThread | undefined) => {
		if (thread && thread.contextValue !== undefined) {
			objs.refreshSingleArtifactThread(thread, false);
		}
		else {
			vscode.window.showErrorMessage("Refresh artifact command failed due to undefined comment thread. This is probably a bug.");
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.refresh-artifact-thread-and-notes', (thread: vscode.CommentThread | undefined) => {
		if (thread && thread.contextValue !== undefined) {
			objs.refreshSingleArtifactThread(thread, true);
		}
		else {
			vscode.window.showErrorMessage("Refresh artifact and notes command failed due to undefined comment thread. This is probably a bug.");
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.copy-artifact-from-comment-thread', (commentThread: vscode.CommentThread | undefined) => {
		if (commentThread && commentThread.contextValue !== undefined) {
			var artifactId = commentThread.contextValue;
			var lociClient = utils.getLociClient();
			lociSemaphore.acquire().then(function ([value, release]) {
				lociClient.readArtifact(Number(artifactId)).then(function (response) {
					var artifact = response.data as ArtifactOut;
					vscode.env.clipboard.writeText(artifact.descriptor);
					vscode.window.showInformationMessage("Copied '" + artifact.descriptor + "' to clipboard.");
				}).finally(() => {
					release();
				});
			});
		}
		else {
			vscode.window.showErrorMessage("Copy artifact command failed due to undefined comment thread. This is probably a bug.");
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.copy-artifact-from-comment-thread-markdown', (commentThread: vscode.CommentThread | undefined) => {
		if (commentThread && commentThread.contextValue !== undefined) {
			var artifactId = commentThread.contextValue;
			var lociClient = utils.getLociClient();
			lociSemaphore.acquire().then(function ([value, release]) {
				lociClient.readArtifact(Number(artifactId)).then(function (response) {
					var artifact = response.data as ArtifactOut;
					vscode.env.clipboard.writeText("[" + artifact.descriptor + "](" + artifact.descriptor + ")");
					vscode.window.showInformationMessage("Copied '" + artifact.descriptor + "' link to clipboard.");
				}).finally(() => {
					release();
				});
			});
		}
		else {
			vscode.window.showErrorMessage("Copy artifact command failed due to undefined comment thread. This is probably a bug.");
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.copy-artifact', () => {
		if (vscode.window.activeTextEditor) {
			var filename = utils.openDocumentToArtifactFilename();
			var startRange = vscode.window.activeTextEditor.selection.start;
			var endRange = vscode.window.activeTextEditor.selection.end;

			var lines = "";
			if (startRange.line === endRange.line) {
				lines = (startRange.line + 1).toString();
			}
			else {
				lines = (startRange.line + 1) + "-" + (endRange.line + 1);
			}

			var location = filename + ":" + lines;
			vscode.env.clipboard.writeText(location);
			vscode.window.showInformationMessage("Copied artifact '" + location + "' link to clipboard.");
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.copy-artifact-markdown', () => {
		if (vscode.window.activeTextEditor) {
			var filename = utils.openDocumentToArtifactFilename();
			var startRange = vscode.window.activeTextEditor.selection.start;
			var endRange = vscode.window.activeTextEditor.selection.end;

			var lines = "";
			if (startRange.line === endRange.line) {
				lines = (startRange.line + 1).toString();
			}
			else {
				lines = (startRange.line + 1) + "-" + (endRange.line + 1);
			}

			var location = filename + ":" + lines;
			vscode.env.clipboard.writeText("[" + location + "](" + location + ")");
			vscode.window.showInformationMessage("Copied artifact '" + location + "' link to clipboard.");
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.raise-priority', async (commentThread: vscode.CommentThread) => {
		if (commentThread.contextValue !== undefined) {
			// This is idiotic, but I'll fix it better later.
			var priority = commentThread.label?.split("]")?.[0].split("[")?.[1].split(" ")[0];
			if (priority === undefined) {
				vscode.window.showErrorMessage("Failed to raise priority due to undefined priority. This is probably a bug.");
				return;
			}
			utils.raisePriority(parseInt(commentThread.contextValue), priority, commentThread);
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.lower-priority', async (commentThread: vscode.CommentThread) => {
		if (commentThread.contextValue !== undefined) {
			// This is idiotic, but I'll fix it better later.
			var priority = commentThread.label?.split("]")?.[0].split("[")?.[1].split(" ")[0];
			if (priority === undefined) {
				vscode.window.showErrorMessage("Failed to lower priority due to undefined priority. This is probably a bug.");
				return;
			}
			utils.lowerPriority(parseInt(commentThread.contextValue), priority, commentThread);
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.set-priority-done', async (commentThread: vscode.CommentThread) => {
		if (commentThread.contextValue !== undefined) {
			utils.setPriorityDone(parseInt(commentThread.contextValue), commentThread);
		}
	});

	vscode.commands.registerCommand('loci-notes-vs-code.delete-note', async (noteComment: objs.NoteComment) => {
		if (noteComment !== undefined && noteComment.id !== undefined && noteComment.id !== -1) {
			var noteId = noteComment.id;
			var parentThread = noteComment.parent;
			var lociClient = utils.getLociClient();
			var shortBody = noteComment.body.value;
			if (shortBody.length > 50) {
				shortBody = shortBody.substring(0, 50) + "...";
			}

			lociSemaphore.acquire().then(function ([value, release]) {
				lociClient.deleteNote(Number(noteId)).then(function (response) {
					vscode.window.showInformationMessage("Deleted '" + shortBody + "'.");
					// We need to refresh the parent thread to remove the note from the view.
					vscode.commands.executeCommand("loci-notes-vs-code.refresh-artifact-thread-and-notes", parentThread);
				}).finally(() => {
					release();
				});
			});
		}
	});

	// For some reason using vscode.open in other places doesn't actually work correctly. Not sure why, too tired to debug. This works, and adds some usability to boot.
	vscode.commands.registerCommand('loci-notes-vs-code.open-artifact', (artifact: ArtifactOut) => {
		vscode.commands.executeCommand("vscode.open", objs.getArtifactLocalUri(artifact)).then(() => {
			// All of the following would be a great idea, if only `revealLine` took into account the comments length, which it currently does not.
			// This is a bit of a hack, but we instruct VSCode to center the view on the cursor, since in some cases the "open" command will not be centered because comments above where we want to be cause the view to be pushed down, (even when comments are preloaded, might file a bug with VS Code).
			//let currentLineNumber = vscode.window.activeTextEditor?.selection.start.line;
			//vscode.commands.executeCommand("revealLine", {lineNumber: currentLineNumber, at: "center"});
		});
	});

	// Same as above, but takes a string instead of an ArtifactOut.
	vscode.commands.registerCommand('loci-notes-vs-code.open-artifact-str', (artifact: string) => {
		vscode.commands.executeCommand("vscode.open", objs.getArtifactLocalUriByString(artifact)).then(() => {
		});
	});

	vscode.commands.registerCommand('loci-notes-vs-code.comment-search-results', () => {
		const activeDocument = vscode.window.activeTextEditor?.document;
		if (!activeDocument || activeDocument.languageId !== 'search-result') {
			return;
		}

		const lines = activeDocument.getText().split('\n');
		let artifactFilename: string | undefined;
		var lastWasResult = false;
		var artifacts = [] as string[];

		// Currently, there is no API to retrieve the results of a search from the search editor window. However, in he editor, lines which match have a ":" at the end of the line, and the line number is the first word in the line. We can use this to extract the file name and line number of the search result, thus building the artifact.

		for (const line of lines) {
			const fileLine = FILE_LINE_REGEX.exec(line);
			if (fileLine) {
				const [, path] = fileLine;
				artifactFilename = utils.getArtifactFilename(path, false);
			}

			if (!artifactFilename) {
				continue;
			}

			const resultLine = RESULT_LINE_REGEX.exec(line);
			if (resultLine && !lastWasResult) {
				lastWasResult = true;
				const [, indentation, lineNumber, seperator, newLine] = resultLine;
				let lineNumberN = parseInt(lineNumber);
				const artifact = artifactFilename + ":" + lineNumberN;

				artifacts.push(artifact);
			} else {
				lastWasResult = false;
			}
		}

		var prioritySelection: string | undefined = undefined;
		const priorityQP = vscode.window.createQuickPick();
		priorityQP.items = [
			{
				"label": utils.priorityToUserString("HIGH"),
				"description": "Set all selected artifacts to high priority.",
			},
			{
				"label": utils.priorityToUserString("MEDIUM"),
				"description": "Set all selected artifacts to medium priority.",
			},
			{
				"label": utils.priorityToUserString("LOW"),
				"description": "Set all selected artifacts to low priority.",
			},
			{
				"label": utils.priorityToUserString("NONE"),
				"description": "Set all selected artifacts to completed (no priority).",
			},
		];
		priorityQP.step = 1;
		priorityQP.canSelectMany = false;
		priorityQP.totalSteps = 2;
		priorityQP.title = "Select Priority";
		priorityQP.matchOnDescription = true;
		priorityQP.matchOnDetail = true;

		priorityQP.onDidChangeSelection(([selection]) => {
			if (!selection) {
				return;
			}

			prioritySelection = selection.label;

			let inputBox = vscode.window.createInputBox();
			inputBox.prompt = "Enter a comment for the selected artifacts.";
			inputBox.placeholder = "Comment...";
			inputBox.onDidAccept(async () => {
				if (inputBox.value === undefined || inputBox.value === "") {
					inputBox.validationMessage = "Comment cannot be empty.";
					return;
				}
				inputBox.busy = true;
				inputBox.enabled = false;

				const comment = inputBox.value;
				if (comment === undefined) {
					vscode.window.showErrorMessage("Failed to add comment to search selections due to undefined comment. This is probably a bug.");
					return;
				}
				for (var n = 0; n < artifacts.length; n++) {
					var artifact = artifacts[n];
					// console.log(selection.label + " " + artifact + " - " + comment);

					// Just grab the comment thread, if it exists.
					let commentThread = await getCommentThread(artifact);

					if (commentThread === undefined) {
						vscode.window.showErrorMessage(`Failed to add comment from artifact '${artifact}'. This is probably a bug.`);
						continue;
					}

					commentThread.state = vscode.CommentThreadState.Unresolved;
					commentThread.collapsibleState = vscode.CommentThreadCollapsibleState.Expanded;
					commentThread.label = "";

					let commentReply: vscode.CommentReply = {
						thread: commentThread,
						text: comment,
					};
					if (prioritySelection === undefined) {
						vscode.window.showErrorMessage("Failed to add comment due to undefined priority. This is probably a bug.");
						return;
					}

					let artifactPriority: ArtifactPriorityEnum = "MEDIUM";
					if (prioritySelection === undefined) {
						vscode.window.showErrorMessage("Failed to add comment due to undefined priority string. This is probably a bug.");
						artifactPriority = ArtifactPriorityEnum.Medium;
					} else if (prioritySelection === "High priority") {
						artifactPriority = "HIGH";
					} else if (prioritySelection === "Low priority") {
						artifactPriority = "LOW";
					} else if (prioritySelection === "Done") {
						artifactPriority = "NONE";
					}

					// There's a bug somewhere where SOME (but not all) of the threads will refresh correctly. TODO figure out why and fix it, might have something to do with the async nature of how notes are added and then refreshed. 
					await objs.addNote(commentReply, artifactPriority);
				}
				inputBox.busy = false;

				// This is idiotic, but I can't figure out how to get the comment thread to update without this.
				vscode.commands.executeCommand('loci-notes-vs-code.refresh-artifacts');

				vscode.window.showInformationMessage(`Added comment to ${artifacts.length} selected artifacts.`);
				inputBox.dispose();
			});
			inputBox.show();
			priorityQP.dispose();
		});
		priorityQP.show();
	});

	vscode.commands.registerCommand('loci-notes-vs-code.comment-navstack', () => {
		if (navStack.isEmpty()) {
			vscode.window.showInformationMessage("Navigation stack is empty.");
			return;
		}

		let artifacts = navStack.getItems();

		var prioritySelection: string | undefined = undefined;

		const priorityQP = vscode.window.createQuickPick();
		priorityQP.items = [
			{
				"label": utils.priorityToUserString("HIGH"),
				"description": "Set all selected artifacts to high priority.",
			},
			{
				"label": utils.priorityToUserString("MEDIUM"),
				"description": "Set all selected artifacts to medium priority.",
			},
			{
				"label": utils.priorityToUserString("LOW"),
				"description": "Set all selected artifacts to low priority.",
			},
			{
				"label": utils.priorityToUserString("NONE"),
				"description": "Set all selected artifacts to completed (no priority).",
			},
		];
		priorityQP.step = 1;
		priorityQP.canSelectMany = false;
		priorityQP.totalSteps = 2;
		priorityQP.title = "Select Priority";
		priorityQP.matchOnDescription = true;
		priorityQP.matchOnDetail = true;

		priorityQP.onDidChangeSelection(([selection]) => {
			if (!selection) {
				return;
			}
			
			prioritySelection = selection.label;

			let inputBox = vscode.window.createInputBox();
			inputBox.prompt = "Enter a comment for the selected artifacts.";
			inputBox.placeholder = "Comment...";
			inputBox.onDidAccept(async () => {
				if (inputBox.value === undefined || inputBox.value === "") {
					inputBox.validationMessage = "Comment cannot be empty.";
					return;
				}
				inputBox.busy = true;
				inputBox.enabled = false;

				const comment = inputBox.value;
				if (comment === undefined) {
					vscode.window.showErrorMessage("Failed to add comment to Navigation Stack locations due to undefined comment. This is probably a bug.");
					return;
				}
				for (var n = 0; n < artifacts.length; n++) {
					var artifact = artifacts[n];
					// console.log(selection.label + " " + artifact + " - " + comment);

					// Just grab the comment thread, if it exists.
					let commentThread = await getCommentThread(artifact);

					if (commentThread === undefined) {
						vscode.window.showErrorMessage(`Failed to add comment from artifact '${artifact}'. This is probably a bug.`);
						continue;
					}

					commentThread.state = vscode.CommentThreadState.Unresolved;
					commentThread.collapsibleState = vscode.CommentThreadCollapsibleState.Expanded;
					commentThread.label = "";

					let commentReply: vscode.CommentReply = {
						thread: commentThread,
						text: comment,
					};
					if (prioritySelection === undefined) {
						vscode.window.showErrorMessage("Failed to add comment due to undefined priority. This is probably a bug.");
						return;
					}

					let artifactPriority: ArtifactPriorityEnum = "MEDIUM";
					if (prioritySelection === undefined) {
						vscode.window.showErrorMessage("Failed to add comment due to undefined priority string. This is probably a bug.");
						artifactPriority = ArtifactPriorityEnum.Medium;
					} else if (prioritySelection === "High priority") {
						artifactPriority = "HIGH";
					} else if (prioritySelection === "Low priority") {
						artifactPriority = "LOW";
					} else if (prioritySelection === "Done") {
						artifactPriority = "NONE";
					}

					// Insert a special comment in the artifact to indicate that this comment was added from the nav stack, and link the prev and next item for each artifact.
					var linkComment = `[${n + 1}/${artifacts.length}] Navstack - `;
					if (n > 0) {
						// This is the previous artifact.
						var prevArtifact = artifacts[n - 1];
						linkComment += `[Previous](${prevArtifact})`;
					}
					if (n < artifacts.length - 1) {
						if (n > 0) {
							linkComment += " | ";
						}

						// This is the next artifact.
						var nextArtifact = artifacts[n + 1];
						
						linkComment += ` [Next](${nextArtifact})`;
					}
					let linkCommentReply: vscode.CommentReply = {
						thread: commentThread,
						text: linkComment,
					};
					await objs.addNote(linkCommentReply, artifactPriority);

					// There's a bug somewhere where SOME (but not all) of the threads will refresh correctly. TODO figure out why and fix it, might have something to do with the async nature of how notes are added and then refreshed. 
					await objs.addNote(commentReply, artifactPriority);
				}
				inputBox.busy = false;

				// This is idiotic, but I can't figure out how to get the comment thread to update without this.
				vscode.commands.executeCommand('loci-notes-vs-code.refresh-artifacts');

				vscode.window.showInformationMessage(`Added comment to ${artifacts.length} selected artifacts.`);
				inputBox.dispose();
			});
			inputBox.show();
			priorityQP.dispose();
		});
		priorityQP.show();
	});


	// This code runs on activation, and currently the only thing that "activates" this 
	// ext is the presence of a Loci Notes Project file.
	// This next lines gets the current information about the server and project.
	if (vscode.workspace.workspaceFolders !== undefined) {
		vscode.commands.executeCommand('loci-notes-vs-code.check-project');
		vscode.commands.executeCommand('loci-notes-vs-code.refresh-artifacts');
	}
	else {
		// We should never be here.
		vscode.window.showErrorMessage('Loci Notes Project File could not be found. This is probably a bug.');
		deactivate();
	}

	function newCommentController() {
		const commentController = vscode.comments.createCommentController('comment-loci-notes', 'Loci Notes');
		context.subscriptions.push(commentController);

		// A `CommentingRangeProvider` controls where gutter decorations that allow adding comments are shown
		commentController.commentingRangeProvider = {
			provideCommentingRanges: (document: vscode.TextDocument, token: vscode.CancellationToken) => {
				// We have to check if the document is in the source directory. If not, we don't want to show the comment icon in the gutter, as it falls outside of the Loci Notes source code directory.
				var docPath = document.uri.fsPath;
				var splitPath = docPath.split(path.sep);
				if (splitPath.includes(utils.getLociSourceDirectoryName())) {
					const lineCount = document.lineCount;
					return [new vscode.Range(0, 0, lineCount - 1, 0)];
				}
				return [];
			}
		};
		commentController.options = { placeHolder: "Add a note...", prompt: "Add a note..." };

		return commentController;
	}

	// This function will insert a comment thread into the document in the VS Code editor for the given artifact.
	async function getCommentThread(artifact: string): Promise<vscode.CommentThread | undefined> {
		var startLine = objs.getArtifactLineStartByString(artifact);
		if (startLine === undefined) {
			vscode.window.showErrorMessage(`The artifact descriptor '${artifact}' is returning an error when attempting to read the starting line. This is probably a bug.`);
			return Promise.resolve(undefined);
		}

		var endLine: number = startLine;
		var tmpEndline = objs.getArtifactLineEndByString(artifact);
		if (tmpEndline !== undefined) {
			endLine = tmpEndline;
		}

		var startPosition = new vscode.Position(startLine - 1, 0);
		var endPosition = new vscode.Position(endLine - 1, 0);

		var range = new vscode.Range(startPosition, endPosition);
		var artifactUri = objs.getArtifactLocalUriByString(artifact, false);
		if (artifactUri === undefined) {
			return Promise.resolve(undefined);
		}

		let thread = commentController.createCommentThread(artifactUri, range, []);
		return Promise.resolve(thread);
	}

	function refreshAllFileArtifacts() {
		crud.getAllLocations().then(async function (artifacts) {
			// It's a good API call, remove all previous notes (we are rebuilding them from scratch).

			// It's a bit stupid that we have to dispose of the entire controller and create a brand new one, but there's not a good way to remove previously inserted comments cleanly.
			commentController.dispose();
			commentController = newCommentController();

			for (var n = 0; n < artifacts.length; n++) {
				var artifact = artifacts[n];
				var thread = await getCommentThread(artifact.descriptor);

				// If we failed to insert the comment thread, we should probably just skip this artifact.
				if (thread === undefined) {
					vscode.window.showErrorMessage(`Failed to insert comment thread for artifact '${artifact.descriptor}'. This is probably a bug.`);
					continue;
				}

				let artifactTmp = artifact;
				var artifactPriorityStr = utils.priorityToUserString(artifact.priority.toString());
				thread.label = `[${artifactPriorityStr}] ${artifactTmp.descriptor}`;

				// As of 1.60, there is no good way to save the per-thread expansion state, or to intercept the "expand" and "collapse" clicks in the gutter that can be saved and then recreated when the Comment Controller is rebuilt. In the future, if this API is created, this should be fixed.
				thread.collapsibleState = vscode.CommentThreadCollapsibleState.Expanded;

				// We need the specific artifact ID that each thread is attached to, so we just store it in this all-purpose area.
				thread.contextValue = artifactTmp.id.toString();

				// Finally, issue a command to refresh this artifact thread.
				vscode.commands.executeCommand("loci-notes-vs-code.refresh-artifact-thread-and-notes", thread);
			}
		});
	}
}

// this method is called when your extension is deactivated
export function deactivate() {
	if (disposables) {
		disposables.forEach(item => item.dispose());
	}
	disposables = [];
}
