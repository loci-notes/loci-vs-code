import * as vscode from 'vscode';
import * as path from 'path';
import { NavStack } from './NavStack';

export class NavStackView implements vscode.TreeDataProvider<NavBookmark> {
    constructor(private navStack: NavStack) { }

    getTreeItem(element: NavBookmark): vscode.TreeItem {
        return element;
    }

    getChildren(element?: NavBookmark): Thenable<NavBookmark[]> {
        var navBookmarkList: NavBookmark[] = [];
        var navStackItems = this.navStack.getItems();
        for (var i = 0; i < navStackItems.length; i++) {
            var navBookmark = new NavBookmark(navStackItems[i]);
            navBookmarkList.push(navBookmark);
        }
        return Promise.resolve(navBookmarkList);
    }

    private _onDidChangeTreeData: vscode.EventEmitter<NavBookmark | undefined | null | void> = new vscode.EventEmitter<NavBookmark | undefined | null | void>();

    readonly onDidChangeTreeData: vscode.Event<NavBookmark | undefined | null | void> = this._onDidChangeTreeData.event;

    refresh(): void {
        this._onDidChangeTreeData.fire();
    }
}

class NavBookmark extends vscode.TreeItem {
    constructor(
        public readonly location: string,
    ) {
        var splitPath = location.split("/");
        super(splitPath[splitPath.length - 1], vscode.TreeItemCollapsibleState.None);
        this.tooltip = location;
        this.description = location;
        if (location !== undefined) {
            // cmdArg should hold the full artifact descriptor
            this.command = {
                command: 'loci-notes-vs-code.open-artifact-str',
                title: '',
                arguments: [location]
            };
        }
    }

    iconPath = {
        light: path.join(__filename, "..", "..", "docs", "imgs", "bookmark-dark.svg"),
        dark: path.join(__filename, "..", "..", "docs", "imgs", "bookmark-light.svg")
    };
}