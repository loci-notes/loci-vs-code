import * as fs from 'fs';
import * as pathUtils from 'path';
import * as process from 'process';
import * as vscode from 'vscode';
import * as client from './client';
import * as objs from './objs';
import dayjs from 'dayjs';
import { Semaphore } from 'async-mutex';
import axios from 'axios';

const ini = require('ini');

// TODO Should probably make this a setting.
// This is roughly the limit for what we can handle against a t3a.medium instance with HTTPS. Might need to drop it a bit for project with multiple users.
const API_CALL_POOL_LIMIT = 32;
// This semaphore is used to limit the number of concurrent API calls to the Loci server. Without this, we could easily overwhelm the server with too many requests, usually showing up as a 500 error, and a "sqlalchemy.exc.TimeoutError: QueuePool limit" in the server logs. See https://github.com/DirtyHairy/async-mutex?tab=readme-ov-file#manual-locking--releasing-1 for how this works.
export const lociSemaphore = new Semaphore(API_CALL_POOL_LIMIT);

function joinURLs(baseURL: string, relativeURL: string) {
    return relativeURL ? baseURL.replace(/\/+$/, '') + '/' + relativeURL.replace(/^\/+/, '') : baseURL;
}

// This function from https://github.com/tensorflow/tensorboard/blob/master/tensorboard/uploader/util.py
// Translated to JS by TheTwitchy
// Apache 2 licensed
export function getUserConfigDirectory(): string {
    if (process.platform === 'win32') {
        var appdata = process.env.LOCALAPPDATA;
        if (appdata !== undefined) {
            return appdata;
        }
        appdata = process.env.APPDATA;
        if (appdata !== undefined) {
            return appdata;
        }
    }
    var xdgConfigHome = process.env.XDG_CONFIG_HOME;
    if (xdgConfigHome !== undefined) {
        return xdgConfigHome;
    }

    if (process.platform === 'darwin') {
        return pathUtils.join(require('os').homedir(), "Library", "Application Support").normalize();
    }

    return pathUtils.join(require('os').homedir(), ".config").normalize();
}

export function getLociDirectory() {
    return pathUtils.join(getUserConfigDirectory(), "loci").normalize();
}

export function getLociConfig() {
    return ini.parse(fs.readFileSync(pathUtils.join(getLociDirectory(), 'loci-config.ini').normalize(), 'utf-8'));
}

export function escapeRegExp(str: string) {
    // From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

export function getProjectId(): number {
    // Get the project info from the local project file.
    var wf = '';
    if (vscode.workspace.workspaceFolders !== undefined) {
        wf = vscode.workspace.workspaceFolders[0].uri.fsPath;
    }
    var rawProjectInfo = ini.parse(fs.readFileSync(pathUtils.join(wf, '.loci-project.ini').normalize(), 'utf-8'));

    // Get the local project info from the file
    var projectId = rawProjectInfo.default.project_id;
    return parseInt(projectId);
}

export function getProjectServer(): string {
    // Get the project info from the local project file.
    var wf = '';
    if (vscode.workspace.workspaceFolders !== undefined) {
        wf = vscode.workspace.workspaceFolders[0].uri.fsPath;
    }
    var rawProjectInfo = ini.parse(fs.readFileSync(pathUtils.join(wf, '.loci-project.ini').normalize(), 'utf-8'));

    // Get the local project info from the file
    var projectServer = rawProjectInfo.default.server;
    return projectServer;
}

export async function getProjectInfo(): Promise<axios.AxiosResponse<client.ProjectOut>> {
    var projectId = getProjectId();
    var lociClient = getLociClient();

    // Get the project info from the server
    const [value, release] = await lociSemaphore.acquire();
    var projectOut = await lociClient.readProject(projectId);
    release();
    var time = dayjs().format('YYYY-MM-DD HH:mm:ss');
    console.log(`[${time}] Project info received`);

    return projectOut;
}

export function getLociClient(): client.DefaultApi {
    var projectServer = getProjectServer();

    // Get the credentials for the project server
    var localConfig = getLociConfig();
    var clientId = localConfig[projectServer].client_id;
    var secretKey = localConfig[projectServer].secret_key;

    var config = new client.Configuration({
        basePath: projectServer,
        baseOptions: {
            headers: {
                "Authorization": "Bearer " + clientId + ":" + secretKey
            }
        }
    });

    var lociClient: client.DefaultApi = new client.DefaultApi(config);

    return lociClient;
}

export function getLociSourceDirectoryName(): string {
    var folderName: string | undefined = vscode.workspace.getConfiguration().get("loci.root_dir_name");
    if (folderName === undefined) {
        return "_src";
    }
    return folderName;
}

export function getFQFilesIn(folder: string, ignoreFiles: string[] = [], ignoreDirs: string[] = []): string[] {
    var finalResults: string[] = [];

    var resultArr: string[] = fs.readdirSync(folder);

    for (var n = 0; n < resultArr.length; n++) {
        var file = resultArr[n];

        var fqFile: string = pathUtils.join(folder, file);
        var stat = fs.statSync(fqFile);
        if (stat.isDirectory() && !ignoreDirs.includes(file)) {
            // Recurse to get whatever is in this folder.
            var tmp = getFQFilesIn(fqFile, ignoreFiles, ignoreDirs);
            finalResults = finalResults.concat(tmp);
        }
        else if (stat.isFile() && !ignoreFiles.includes(file)) {
            finalResults.push(fqFile);
        }
    }

    return finalResults;
}

/**
 * Transforms a local filename into an artifact filename. Turns a local filename into a filename relative to the source root directory. It also handles Windows-style folder slashes and multi-root workspaces.
 * Examples:
 *   - project/_src/repo/main.cpp -> repo/main.cpp
 * 
 * @param localFilename 
 * @returns 
 */
export function getArtifactFilename(localFilename: string, isChangingNote: boolean = false): string | undefined {
    var fullFilename = pathUtils.normalize(localFilename);
    var currentDir = fullFilename;
    var lastDir = "";
    const srcFolderName = getLociSourceDirectoryName();

    while (currentDir !== lastDir) {
        var base = pathUtils.basename(currentDir);
        if (base === srcFolderName) {
            var ret = fullFilename.substring(currentDir.length + 1);
            // This handles Windows-style folder slashes
            ret = ret.replace(/\\/g, "/");
            return ret;
        }
        lastDir = currentDir;
        currentDir = pathUtils.dirname(currentDir);
    }

    // If we reach here, then we are not under a normal folder of the name getRootDirectoryName(). 
    // We still want to handle this by getting the workspace folder that it is under.
    var workspaces = vscode.workspace.workspaceFolders;
    if (workspaces !== undefined) {
        for (var n = 0; n < workspaces.length; n++) {
            const normalizedFilename = pathUtils.normalize(fullFilename);
            const normalizedWorkspaceDir = pathUtils.normalize(workspaces[n].uri.path);
            if (normalizedFilename.startsWith(normalizedWorkspaceDir)) {
                // Get the artifact name, including the last directory of the workspace folder.
                var pathToRemove = pathUtils.dirname(normalizedWorkspaceDir);
                var finalArtifact = normalizedFilename.substring(pathToRemove.length + 1);
                // This handles Windows-style folder slashes
                finalArtifact = finalArtifact.replace(/\\/g, "/");

                // We only show warnings if we are making changes to a note or artifact, because otherwise this warning will be on every file we open, and will be turned off almost immediately.
                if (isChangingNote && vscode.workspace.getConfiguration().get("loci.show_src_root_warning")) {
                    vscode.window.showWarningMessage("This file does not appear to be under a source root directory called '" + getLociSourceDirectoryName() + "'. You can disable this warning or update the expected source root directory name in Preferences.");
                }
                return finalArtifact;
            }
        }
    }

    return undefined;
}

/**
 * Gives the artifact filename for the currently open document.
 * 
 * @param isChangingNote Tracks if this is needed because we are changing a note or artifact. Some errors are only shown in this case.
 * @returns A string with the artifact filename, or undefined if something goes wrong.
 */
export function openDocumentToArtifactFilename(isChangingNote: boolean = false): string | undefined {
    var openDoc = vscode.window.activeTextEditor?.document;
    if (openDoc === undefined) {
        return undefined;
    }
    return getArtifactFilename(openDoc.uri.path, isChangingNote);
}

export function commentReplyToArtifactFilename(commentReply: vscode.CommentReply, isChangingNote: boolean = false): string | undefined {
    // Get the fully qualified path, like '/home/user/projects/_src/path/to/file' 
    var fqPath = commentReply.thread.uri.path;

    var fullFilename = pathUtils.normalize(fqPath);
    return getArtifactFilename(fullFilename, isChangingNote);
}

export async function raisePriority(artifactId: number, oldPriority: string, thread: vscode.CommentThread) {
    if (!["Low", "Medium", "High", "Done"].includes(oldPriority)) {
        vscode.window.showErrorMessage(`The priority '${oldPriority}' is not valid. This is probably a bug.`);
        return;
    }

    var newPriority: string;
    if (oldPriority === "High") {
        vscode.window.showErrorMessage("Cannot raise the priority of artifact any more.");
        return;
    } else if (oldPriority === "Medium") {
        newPriority = "HIGH";
    } else if (oldPriority === "Low") {
        newPriority = "MEDIUM";
    } else {
        // None to low
        newPriority = "LOW";
    }

    if (thread.label !== undefined) {
        // Interestingly, we can "cheat" and change the label BEFORE the API call, because the API call will take a while, and the user will see the change immediately.
        // This is a bit of a hack, but it's a good user experience.
        var descriptor = thread.label.split("]", 2)[1];
        const newPriorityStr = `${priorityToUserString(newPriority)}`;
        thread.label = "[" + newPriorityStr + "] " + descriptor;
    }

    var artifactUpdate: client.ArtifactIn = {
        priority: newPriority as client.ArtifactPriorityEnum,
    };

    const [value, release] = await lociSemaphore.acquire();
    var lociClient = getLociClient();
    return lociClient.updateArtifact(artifactId, artifactUpdate).then(function (response) {
        // TODO technically, we can use the artifact returned in the response to update the comment thread, but we don't do that yet.
        // Success, update the notes
        vscode.commands.executeCommand("loci-notes-vs-code.refresh-artifact-thread", thread);
    }).finally(() => {
        release();
    });
}

export async function lowerPriority(artifactId: number, oldPriority: string, thread: vscode.CommentThread) {
    if (!["Low", "Medium", "High", "Done"].includes(oldPriority)) {
        vscode.window.showErrorMessage(`The priority '${oldPriority}' is not valid. This is probably a bug.`);
        return;
    }

    var newPriority: string;
    if (oldPriority === "Done") {
        vscode.window.showErrorMessage("Cannot lower the priority of artifact any more.");
        return;
    } else if (oldPriority === "High") {
        newPriority = "MEDIUM";
    } else if (oldPriority === "Medium") {
        newPriority = "LOW";
    } else {
        // Low to none
        newPriority = "NONE";
    }

    if (thread.label !== undefined) {
        // Interestingly, we can "cheat" and change the label BEFORE the API call, because the API call will take a while, and the user will see the change immediately.
        // This is a bit of a hack, but it's a good user experience.
        var descriptor = thread.label.split("]", 2)[1];
        const newPriorityStr = `${priorityToUserString(newPriority)}`;
        thread.label = "[" + newPriorityStr + "] " + descriptor;
    }

    var artifactUpdate: client.ArtifactIn = {
        priority: newPriority as client.ArtifactPriorityEnum,
    };

    var lociClient = getLociClient();
    const [value, release] = await lociSemaphore.acquire();
    return lociClient.updateArtifact(artifactId, artifactUpdate ).then(function (response) {
        // TODO technically, we can use the artifact returned in the response to update the comment thread, but we don't do that yet.

        // Success, update the notes
        vscode.commands.executeCommand("loci-notes-vs-code.refresh-artifact-thread", thread);
    }).finally(() => {
        release();
    });
}

export async function setPriorityDone(artifactId: number, thread: vscode.CommentThread) {
    var newPriority = "NONE";

    if (thread.label !== undefined) {
        // Interestingly, we can "cheat" and change the label BEFORE the API call, because the API call will take a while, and the user will see the change immediately.
        // This is a bit of a hack, but it's a good user experience.
        var descriptor = thread.label.split("]", 2)[1];
        const newPriorityStr = `${priorityToUserString(newPriority)}`;
        thread.label = "[" + newPriorityStr + "] " + descriptor;
    }

    var artifactUpdate: client.ArtifactIn = {
        priority: newPriority as client.ArtifactPriorityEnum,
    };

    var lociClient = getLociClient();
    const [value, release] = await lociSemaphore.acquire();
    return lociClient.updateArtifact(artifactId, artifactUpdate ).then(function (response) {
        // TODO technically, we can use the artifact returned in the response to update the comment thread, but we don't do that yet.

        // Success, update the notes
        vscode.commands.executeCommand("loci-notes-vs-code.refresh-artifact-thread", thread);
    }).finally(() => {
        release();
    });
}

export function uriToString(uri: vscode.Uri | undefined): string {
    if (uri === undefined) {
        return "";
    }

    if (uri.fragment === undefined) {
        return uri.path;
    }

    return uri.path + "#" + uri.fragment;
}


/**
 * This function will take in a string of note contents and process any links in the note. It will replace the links with the full URL to the linked resource that VS Code can understand, rather than the "simple" links that we use for Loci Notes.
 * 
 * Loci Notes links are in the format "[Link Text](path/to/file.txt:1)", with link text first, and the artifact descriptor or code location second. This function will replace them with the full URL to the resource.
 * 
 * @param noteContents 
 * @returns 
 */
export function processNoteLinks(noteContents: string): string {
    const mdLinkRegex = /\[([\w\d\s\.\/:\-]+)\]\(([\w\d\s\.\/:\-]+)\)/g;
    const mdLinkRegexNonGlobal = /\[([\w\d\s\.\/:\-]+)\]\(([\w\d\s\.\/:\-]+)\)/;
    const linkMatches = noteContents.match(mdLinkRegex);

    if (linkMatches === null) {
        // No links to process
        return noteContents;
    }

    // Replace all matches with correct local URIs
    for (var i = 0; i < linkMatches.length; i ++) {
        var linkMatch = linkMatches[i];
        var linkParts = linkMatch.match(mdLinkRegexNonGlobal);
        if (linkParts === null) {
            continue;
        }
        var linkText = linkParts[1];
        var linkPath = linkParts[2];

        // TODO we can probably determine if this is a local file or not better by checking the URI scheme, but we don't do that yet, so we just try to get the local URI.

        var linkUri = objs.getArtifactLocalUriByString(linkPath);
        if (linkUri === undefined) {
            // This is not a local URI, so we can't process it.
            continue;
        }
        // Markdown links cannot have spaces in them. Per spec they must be replaced with %20.
        var linkStr = uriToString(linkUri);
        while (linkStr.indexOf(" ") > -1) {
            linkStr = linkStr.replace(" ", "%20");
        }

        var finalUriLink = "[" + linkText + "](" + linkStr + ")";
        noteContents = noteContents.replace(linkMatches[i], finalUriLink);
    }

    return noteContents;
}

export function capitalize(s: string): string {
    return s[0].toUpperCase() + s.toLowerCase().slice(1);
}

export function priorityToUserString(priority: string): string {
    priority = priority.toUpperCase();
    switch (priority) {
        case "HIGH":
            return "High priority";
        case "MEDIUM":
            return "Medium priority";
        case "LOW":
            return "Low priority";
        case "NONE":
            return "Done";
        default:
            return "Unknown priority";
    }
}