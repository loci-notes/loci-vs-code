export class NavStack {
    private stack: string[] = [];

    push(page: string): void {
        this.stack.push(page);
    }

    pop(): string | undefined {
        return this.stack.pop();
    }

    peek(): string | undefined {
        return this.stack[this.stack.length - 1];
    }

    isEmpty(): boolean {
        return this.stack.length === 0;
    }

    getItems(): string[] {
        return this.stack;
    }
    
}