# Loci Notes VS Code Extension
The official VS Code extension for tracking and interacting with Loci Notes in source code.

## Docs
https://loci-notes.gitlab.io/clients/vs-code

## Requirements
* [Loci Server](https://gitlab.com/loci-notes/loci-server)
* [Loci CLI](https://gitlab.com/loci-notes/loci-cli)
    * Not *technically* required, but you'll need to setup your user account and the project folder either with this tool or manually.

## Known Issues
* This extension is currently in pre-alpha. Things can and will change. See current issues at https://gitlab.com/loci-notes/loci-vs-code/-/issues
